package com.example.profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var etSurname: EditText
    private lateinit var etFirstName: EditText
    private lateinit var etSecondName: EditText
    private lateinit var etAge: EditText
    private lateinit var etHobbies: EditText
    private lateinit var tvWarn: TextView
    private lateinit var btNext: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etSurname = findViewById(R.id.etSurname)
        etFirstName = findViewById(R.id.etFirstName)
        etSecondName = findViewById(R.id.etSecondName)
        etAge = findViewById(R.id.etAge)
        etHobbies = findViewById(R.id.etHobbies)
        tvWarn = findViewById(R.id.tvWarn)
        btNext = findViewById(R.id.btNext)

        var surname = ""
        var firstName = ""
        var secondName = ""
        var age = ""
        var hobbies = ""

        etSurname.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                Log.i("MainActivity", "Surname input: $p0")
                surname = p0.toString()
            }
        })
        etFirstName.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                Log.i("MainActivity", "FirstName input: $p0")
                firstName = p0.toString()
            }
        })

        etSecondName.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                Log.i("MainActivity", "SecondName input: $p0")
                secondName = p0.toString()
            }
        })

        etAge.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                Log.i("MainActivity", "Age input: $p0")
                age = p0.toString()
            }
        })

        etHobbies.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                Log.i("MainActivity", "Hobbies input: $p0")
                hobbies = p0.toString()
            }
        })

        btNext.setOnClickListener{
            if(surname.isNotEmpty() and firstName.isNotEmpty() and secondName.isNotEmpty() and age.isNotEmpty() and hobbies.isNotEmpty()){
                val intent = Intent(this, InfoActivity::class.java)
                intent.putExtra(Constants.firstName, firstName)
                intent.putExtra(Constants.secondName, secondName)
                intent.putExtra(Constants.surname, surname)
                intent.putExtra(Constants.age, age)
                intent.putExtra(Constants.hobbies, hobbies)
                startActivity(intent)
                finish()
            }
            else tvWarn.setVisibility(View.VISIBLE)
        }
    }
}