package com.example.profile

object Constants {
    val firstName = "FNAME"
    val secondName = "SNAME"
    val surname = "SURNAME"
    val age = "AGE"
    val hobbies = "HOBBIES"
}